import socket
import threading

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
maclist=['70:85:c2:fe:4c:55','64:27:37:87:23:f5']

def decrypt(word):
    cont=0
    mensaje=''
    for letter in word:
        cont+=1
    while cont>0:
        cont-=2
        mensaje+=word[cont]
    return mensaje

server.bind(ADDR)

def handle_client(conn, addr):
    i=0
    print(f"[NEW CONNECTION] {addr} connected.")
    connected = True
    print ("Checking MAC Address...")
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == DISCONNECT_MESSAGE:
                connected = False
            if i==0:
                i+=1
                if msg not in maclist:
                    print("MAC Address no válida")
                    connected=False
                if msg in maclist:
                    print("MAC Address reconocida")
        if i!=0:
                if msg not in maclist:
                    print(f"[{addr}] {decrypt(msg)}")
                    conn.send("Msg received".encode(FORMAT))
                if msg in maclist:
                    conn.send("MAC Validada".encode(FORMAT))
    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()
